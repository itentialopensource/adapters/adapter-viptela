
## 0.15.4 [10-15-2024]

* Changes made at 2024.10.14_21:33PM

See merge request itentialopensource/adapters/adapter-viptela!46

---

## 0.15.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-viptela!44

---

## 0.15.2 [08-15-2024]

* Changes made at 2024.08.14_19:52PM

See merge request itentialopensource/adapters/adapter-viptela!43

---

## 0.15.1 [08-07-2024]

* Changes made at 2024.08.06_21:58PM

See merge request itentialopensource/adapters/adapter-viptela!42

---

## 0.15.0 [05-08-2024]

* 2024 Migration - small & manual

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!41

---

## 0.14.5 [03-26-2024]

* Changes made at 2024.03.26_14:12PM

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!40

---

## 0.14.4 [03-13-2024]

* Changes made at 2024.03.13_11:14AM

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!39

---

## 0.14.3 [03-11-2024]

* Changes made at 2024.03.11_10:44AM

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!38

---

## 0.14.2 [02-26-2024]

* Changes made at 2024.02.26_13:07PM

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!37

---

## 0.14.1 [02-21-2024]

* changes for broker calls

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!36

---

## 0.14.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!35

---

## 0.13.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!35

---

## 0.12.3 [09-11-2023]

* Add openapi

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!34

---

## 0.12.2 [09-11-2023]

* more migration & metadata changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!33

---

## 0.12.1 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!32

---

## 0.12.0 [08-16-2023]

* migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!31

---

## 0.11.0 [08-14-2023]

* migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!31

---

## 0.10.8 [08-01-2023]

* Update adapter base and import order

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!28

---

## 0.10.7 [05-15-2023]

* Add template calls and delete duplicated tasks

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!27

---

## 0.10.6 [05-10-2023]

* Migrate adapterBase.js with broker integration changes

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!26

---

## 0.10.5 [04-06-2023]

* Added new calls

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!23

---

## 0.10.4 [03-28-2023]

* Added new calls

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!23

---

## 0.10.3 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!15

---

## 0.10.2 [05-14-2022] & 0.10.1 [05-08-2022] & 0.10.0 [05-01-2022] & 0.9.0 [01-21-2022]

- Added dabout 415 calls

- Multiple versions and releases due to this being one of the adapters heavily involved in the new addapter/broker integration effort.
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!13
See merge request itentialopensource/adapters/sd-wan/adapter-meraki!12
See merge request itentialopensource/adapters/sd-wan/adapter-meraki!11

---

## 0.8.1 [03-16-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!10

---

## 0.8.0 [02-12-2021]

- Adding new calls to the viptela adapter - also migrated to the latest foundation

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!9

---

## 0.7.1 [08-21-2020]

- Add new call for get vsmart policy list

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!8

---

## 0.6.0 [08-14-2020] & 0.7.0 [08-19-2020]

- Add activate/deactivate vSmart policy as well as a way to get the status

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!7

---

## 0.5.3 [08-04-2020]

- Update the authentication in the adapter - request token where the token is returned in a cookie

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!6

---

## 0.5.2 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!5

---

## 0.5.1 [01-15-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!4

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!3

---

## 0.4.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!2

---

## 0.2.0 [07-17-2019] & 0.3.0 [07-30-2019]

- Migrate to the latest adapter foundation, categorize the adapter and prepare for app artifact

See merge request itentialopensource/adapters/sd-wan/adapter-viptela!1

---

## 0.1.1 [04-25-2019]

- Initial Commit

See commit 4a35c51

---
