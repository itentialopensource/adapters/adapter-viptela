/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-viptela',
      type: 'Viptela',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Viptela = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Viptela Adapter Test', () => {
  describe('Viptela Class Tests', () => {
    const a = new Viptela(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('viptela'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('viptela'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Viptela', pronghornDotJson.export);
          assert.equal('Viptela', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-viptela', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('viptela'));
          assert.equal('Viptela', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-viptela', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-viptela', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getStatisticsinterface - errors', () => {
      it('should have a getStatisticsinterface function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatisticsinterface - errors', () => {
      it('should have a postStatisticsinterface function', (done) => {
        try {
          assert.equal(true, typeof a.postStatisticsinterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfaceaggregation - errors', () => {
      it('should have a getStatisticsinterfaceaggregation function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfaceaggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatisticsinterfaceaggregation - errors', () => {
      it('should have a postStatisticsinterfaceaggregation function', (done) => {
        try {
          assert.equal(true, typeof a.postStatisticsinterfaceaggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfacecsv - errors', () => {
      it('should have a getStatisticsinterfacecsv function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfacecsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfacedoccount - errors', () => {
      it('should have a getStatisticsinterfacedoccount function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfacedoccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatisticsinterfacedoccount - errors', () => {
      it('should have a postStatisticsinterfacedoccount function', (done) => {
        try {
          assert.equal(true, typeof a.postStatisticsinterfacedoccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfacefields - errors', () => {
      it('should have a getStatisticsinterfacefields function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfacefields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfacepage - errors', () => {
      it('should have a getStatisticsinterfacepage function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfacepage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatisticsinterfacepage - errors', () => {
      it('should have a postStatisticsinterfacepage function', (done) => {
        try {
          assert.equal(true, typeof a.postStatisticsinterfacepage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsinterfacequeryfields - errors', () => {
      it('should have a getStatisticsinterfacequeryfields function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsinterfacequeryfields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatadevicestatestateDataType - errors', () => {
      it('should have a getDatadevicestatestateDataType function', (done) => {
        try {
          assert.equal(true, typeof a.getDatadevicestatestateDataType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.getDatadevicestatestateDataType(null, null, null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDatadevicestatestateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatadevicestatestateDataTypefields - errors', () => {
      it('should have a getDatadevicestatestateDataTypefields function', (done) => {
        try {
          assert.equal(true, typeof a.getDatadevicestatestateDataTypefields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.getDatadevicestatestateDataTypefields(null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDatadevicestatestateDataTypefields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatadevicestatestateDataTypequery - errors', () => {
      it('should have a getDatadevicestatestateDataTypequery function', (done) => {
        try {
          assert.equal(true, typeof a.getDatadevicestatestateDataTypequery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.getDatadevicestatestateDataTypequery(null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDatadevicestatestateDataTypequery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should have a getDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceconfig - errors', () => {
      it('should have a getDeviceconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceconfig(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceconfightml - errors', () => {
      it('should have a getDeviceconfightml function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceconfightml === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceconfightml(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceconfightml', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicecounters - errors', () => {
      it('should have a getDevicecounters function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicecounters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicedevicestatus - errors', () => {
      it('should have a getDevicedevicestatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicedevicestatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicehardwarehealthdetail - errors', () => {
      it('should have a getDevicehardwarehealthdetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicehardwarehealthdetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicehardwarehealthsummary - errors', () => {
      it('should have a getDevicehardwarehealthsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicehardwarehealthsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicekeyvalue - errors', () => {
      it('should have a getDevicekeyvalue function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicekeyvalue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicemodels - errors', () => {
      it('should have a getDevicemodels function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicemodels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicemodelsuuid - errors', () => {
      it('should have a getDevicemodelsuuid function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicemodelsuuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getDevicemodelsuuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDevicemodelsuuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicemonitor - errors', () => {
      it('should have a getDevicemonitor function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicemonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicequeues - errors', () => {
      it('should have a getDevicequeues function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicequeues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicereachable - errors', () => {
      it('should have a getDevicereachable function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicereachable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicestats - errors', () => {
      it('should have a getDevicestats function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicestats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicestatus - errors', () => {
      it('should have a getDevicestatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicestatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesyncStatus - errors', () => {
      it('should have a getDevicesyncStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesyncStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDevicesyncallmemorydb - errors', () => {
      it('should have a postDevicesyncallmemorydb function', (done) => {
        try {
          assert.equal(true, typeof a.postDevicesyncallmemorydb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicetloc - errors', () => {
      it('should have a getDevicetloc function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicetloc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicetlocutil - errors', () => {
      it('should have a getDevicetlocutil function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicetlocutil === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicetlocutildetail - errors', () => {
      it('should have a getDevicetlocutildetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicetlocutildetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceunreachable - errors', () => {
      it('should have a getDeviceunreachable function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceunreachable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceunreachabledeviceIP - errors', () => {
      it('should have a deleteDeviceunreachabledeviceIP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceunreachabledeviceIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.deleteDeviceunreachabledeviceIP(null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteDeviceunreachabledeviceIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicevedgeinventorydetail - errors', () => {
      it('should have a getDevicevedgeinventorydetail function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicevedgeinventorydetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicevedgeinventorysummary - errors', () => {
      it('should have a getDevicevedgeinventorysummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicevedgeinventorysummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicevmanage - errors', () => {
      it('should have a getDevicevmanage function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicevmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionchangepartition - errors', () => {
      it('should have a getDeviceactionchangepartition function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionchangepartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionchangepartition - errors', () => {
      it('should have a postDeviceactionchangepartition function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionchangepartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactiondefaultpartition - errors', () => {
      it('should have a postDeviceactiondefaultpartition function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactiondefaultpartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionfiltervpn - errors', () => {
      it('should have a getDeviceactionfiltervpn function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionfiltervpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactioninstall - errors', () => {
      it('should have a getDeviceactioninstall function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactioninstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceactioninstall(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactioninstall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactioninstall - errors', () => {
      it('should have a postDeviceactioninstall function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactioninstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactioninstalldevicesdeviceType - errors', () => {
      it('should have a getDeviceactioninstalldevicesdeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactioninstalldevicesdeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.getDeviceactioninstalldevicesdeviceType(null, null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactioninstalldevicesdeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getDeviceactioninstalldevicesdeviceType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactioninstalldevicesdeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionlist - errors', () => {
      it('should have a getDeviceactionlist function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionreboot - errors', () => {
      it('should have a getDeviceactionreboot function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionreboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getDeviceactionreboot(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactionreboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionreboot - errors', () => {
      it('should have a postDeviceactionreboot function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionreboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionrebootdevicesdeviceType - errors', () => {
      it('should have a getDeviceactionrebootdevicesdeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionrebootdevicesdeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.getDeviceactionrebootdevicesdeviceType(null, null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactionrebootdevicesdeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getDeviceactionrebootdevicesdeviceType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceactionrebootdevicesdeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionrediscover - errors', () => {
      it('should have a getDeviceactionrediscover function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionrediscover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionrediscover - errors', () => {
      it('should have a postDeviceactionrediscover function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionrediscover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDeviceactionrediscover(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-postDeviceactionrediscover', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionrediscoverall - errors', () => {
      it('should have a postDeviceactionrediscoverall function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionrediscoverall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionremovepartition - errors', () => {
      it('should have a getDeviceactionremovepartition function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionremovepartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionremovepartition - errors', () => {
      it('should have a postDeviceactionremovepartition function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionremovepartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionstartmonitor - errors', () => {
      it('should have a getDeviceactionstartmonitor function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionstartmonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionuniquevpnlist - errors', () => {
      it('should have a postDeviceactionuniquevpnlist function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionuniquevpnlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionvpn - errors', () => {
      it('should have a getDeviceactionvpn function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionvpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionztpupgrade - errors', () => {
      it('should have a getDeviceactionztpupgrade function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionztpupgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionztpupgrade - errors', () => {
      it('should have a postDeviceactionztpupgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionztpupgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceactionztpupgradesetting - errors', () => {
      it('should have a getDeviceactionztpupgradesetting function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceactionztpupgradesetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDeviceactionztpupgradesetting - errors', () => {
      it('should have a postDeviceactionztpupgradesetting function', (done) => {
        try {
          assert.equal(true, typeof a.postDeviceactionztpupgradesetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateVsmartPolicy - errors', () => {
      it('should have a activateVsmartPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.activateVsmartPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activateVsmartPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-activateVsmartPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateVsmartPolicy - errors', () => {
      it('should have a deactivateVsmartPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateVsmartPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deactivateVsmartPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deactivateVsmartPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceVsmartStatus - errors', () => {
      it('should have a getDeviceVsmartStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceVsmartStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getDeviceVsmartStatus(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceVsmartStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVsmartPolicyList - errors', () => {
      it('should have a getVsmartPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getVsmartPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceList - errors', () => {
      it('should have a getDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterTemplateId', (done) => {
        try {
          a.getDeviceList(null, (data, error) => {
            try {
              const displayE = 'masterTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushMixTemplate - errors', () => {
      it('should have a pushMixTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.pushMixTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedConfigToDevice - errors', () => {
      it('should have a getAttachedConfigToDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachedConfigToDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushMasterTemplate - errors', () => {
      it('should have a pushMasterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.pushMasterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedDeviceList - errors', () => {
      it('should have a getAttachedDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachedDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterTemplateId', (done) => {
        try {
          a.getAttachedDeviceList(null, (data, error) => {
            try {
              const displayE = 'masterTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAttachedDeviceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInputWithoutDevice - errors', () => {
      it('should have a createInputWithoutDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createInputWithoutDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfigurationPreview - errors', () => {
      it('should have a getDeviceConfigurationPreview function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceConfigurationPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processInputCommaSepFile - errors', () => {
      it('should have a processInputCommaSepFile function', (done) => {
        try {
          assert.equal(true, typeof a.processInputCommaSepFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateTemplate - errors', () => {
      it('should have a validateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.validateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushCloudxConfig - errors', () => {
      it('should have a pushCloudxConfig function', (done) => {
        try {
          assert.equal(true, typeof a.pushCloudxConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCloudxConfig - errors', () => {
      it('should have a editCloudxConfig function', (done) => {
        try {
          assert.equal(true, typeof a.editCloudxConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachSites - errors', () => {
      it('should have a detachSites function', (done) => {
        try {
          assert.equal(true, typeof a.detachSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesWithDuplicateIP - errors', () => {
      it('should have a getDevicesWithDuplicateIP function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesWithDuplicateIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushCLITemplate - errors', () => {
      it('should have a pushCLITemplate function', (done) => {
        try {
          assert.equal(true, typeof a.pushCLITemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachDeviceTemplate - errors', () => {
      it('should have a detachDeviceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.detachDeviceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceInput - errors', () => {
      it('should have a createDeviceInput function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceInput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkVbond - errors', () => {
      it('should have a checkVbond function', (done) => {
        try {
          assert.equal(true, typeof a.checkVbond === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateTemplateList - errors', () => {
      it('should have a generateTemplateList function', (done) => {
        try {
          assert.equal(true, typeof a.generateTemplateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplate - errors', () => {
      it('should have a createTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplate - errors', () => {
      it('should have a getTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getTemplate(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplate - errors', () => {
      it('should have a editTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.editTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplate - errors', () => {
      it('should have a deleteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteTemplate(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editLITemplate - errors', () => {
      it('should have a editLITemplate function', (done) => {
        try {
          assert.equal(true, typeof a.editLITemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.editLITemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editLITemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLITemplate - errors', () => {
      it('should have a createLITemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createLITemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLITemplate - errors', () => {
      it('should have a listLITemplate function', (done) => {
        try {
          assert.equal(true, typeof a.listLITemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateTemplateByDeviceType - errors', () => {
      it('should have a generateTemplateByDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.generateTemplateByDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.generateTemplateByDeviceType(null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateTemplateByDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDefinition - errors', () => {
      it('should have a getTemplateDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getTemplateDefinition(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTemplateDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateTemplateTypes - errors', () => {
      it('should have a generateTemplateTypes function', (done) => {
        try {
          assert.equal(true, typeof a.generateTemplateTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateTemplateTypeDefinition - errors', () => {
      it('should have a generateTemplateTypeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.generateTemplateTypeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeName', (done) => {
        try {
          a.generateTemplateTypeDefinition(null, null, (data, error) => {
            try {
              const displayE = 'typeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateTemplateTypeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.generateTemplateTypeDefinition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateTemplateTypeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateMasterTemplateDefinition - errors', () => {
      it('should have a generateMasterTemplateDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.generateMasterTemplateDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeName', (done) => {
        try {
          a.generateMasterTemplateDefinition(null, (data, error) => {
            try {
              const displayE = 'typeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateMasterTemplateDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTemplatesAttachedToFeature - errors', () => {
      it('should have a getDeviceTemplatesAttachedToFeature function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceTemplatesAttachedToFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getDeviceTemplatesAttachedToFeature(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceTemplatesAttachedToFeature', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetVedgeCloud - errors', () => {
      it('should have a resetVedgeCloud function', (done) => {
        try {
          assert.equal(true, typeof a.resetVedgeCloud === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.resetVedgeCloud(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-resetVedgeCloud', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateBootstrapConfigForVedges - errors', () => {
      it('should have a generateBootstrapConfigForVedges function', (done) => {
        try {
          assert.equal(true, typeof a.generateBootstrapConfigForVedges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decommissionVedgeCloud - errors', () => {
      it('should have a decommissionVedgeCloud function', (done) => {
        try {
          assert.equal(true, typeof a.decommissionVedgeCloud === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.decommissionVedgeCloud(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-decommissionVedgeCloud', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateBootstrapConfigForVedge - errors', () => {
      it('should have a generateBootstrapConfigForVedge function', (done) => {
        try {
          assert.equal(true, typeof a.generateBootstrapConfigForVedge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.generateBootstrapConfigForVedge(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateBootstrapConfigForVedge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBootstrapConfigZip - errors', () => {
      it('should have a getBootstrapConfigZip function', (done) => {
        try {
          assert.equal(true, typeof a.getBootstrapConfigZip === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getBootstrapConfigZip(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getBootstrapConfigZip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#formPost - errors', () => {
      it('should have a formPost function', (done) => {
        try {
          assert.equal(true, typeof a.formPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesDetails - errors', () => {
      it('should have a getDevicesDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceCategory', (done) => {
        try {
          a.getDevicesDetails(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceCategory is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDevicesDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDevice - errors', () => {
      it('should have a createDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagementSystemIPInfo - errors', () => {
      it('should have a getManagementSystemIPInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getManagementSystemIPInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevice - errors', () => {
      it('should have a deleteDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteDevice(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDevice - errors', () => {
      it('should have a editDevice function', (done) => {
        try {
          assert.equal(true, typeof a.editDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.editDevice(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControllerVEdgeSyncStatus - errors', () => {
      it('should have a getControllerVEdgeSyncStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getControllerVEdgeSyncStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootCertStatusAll - errors', () => {
      it('should have a getRootCertStatusAll function', (done) => {
        try {
          assert.equal(true, typeof a.getRootCertStatusAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#syncDevices - errors', () => {
      it('should have a syncDevices function', (done) => {
        try {
          assert.equal(true, typeof a.syncDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudDockDataBasedOnDeviceType - errors', () => {
      it('should have a getCloudDockDataBasedOnDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudDockDataBasedOnDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceCategory', (done) => {
        try {
          a.getCloudDockDataBasedOnDeviceType(null, (data, error) => {
            try {
              const displayE = 'deviceCategory is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCloudDockDataBasedOnDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutOfSyncTemplates - errors', () => {
      it('should have a getOutOfSyncTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getOutOfSyncTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOutOfSyncDevices - errors', () => {
      it('should have a getOutOfSyncDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getOutOfSyncDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOutOfSyncDevices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getOutOfSyncDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMasterTemplate - errors', () => {
      it('should have a createMasterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createMasterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMasterTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-createMasterTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCLITemplate - errors', () => {
      it('should have a createCLITemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createCLITemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCLITemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-createCLITemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMasterTemplateDefinition - errors', () => {
      it('should have a getMasterTemplateDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getMasterTemplateDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getMasterTemplateDefinition(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getMasterTemplateDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateMasterTemplateList - errors', () => {
      it('should have a generateMasterTemplateList function', (done) => {
        try {
          assert.equal(true, typeof a.generateMasterTemplateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing feature', (done) => {
        try {
          a.generateMasterTemplateList(null, (data, error) => {
            try {
              const displayE = 'feature is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateMasterTemplateList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editMasterTemplate - errors', () => {
      it('should have a editMasterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.editMasterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.editMasterTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editMasterTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVSmartTemplate - errors', () => {
      it('should have a createVSmartTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createVSmartTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateByPolicyId - errors', () => {
      it('should have a getTemplateByPolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateByPolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getTemplateByPolicyId(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTemplateByPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVSmartTemplate - errors', () => {
      it('should have a editVSmartTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.editVSmartTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.editVSmartTemplate(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editVSmartTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVSmartTemplate - errors', () => {
      it('should have a deleteVSmartTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVSmartTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteVSmartTemplate(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteVSmartTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRunningConfig - errors', () => {
      it('should have a getRunningConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getRunningConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.getRunningConfig(null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getRunningConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadConfig - errors', () => {
      it('should have a uploadConfig function', (done) => {
        try {
          assert.equal(true, typeof a.uploadConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.uploadConfig(null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-uploadConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachedConfig - errors', () => {
      it('should have a getAttachedConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachedConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.getAttachedConfig(null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAttachedConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigDiff - errors', () => {
      it('should have a getConfigDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.getConfigDiff(null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getConfigDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateCLIModeDevices - errors', () => {
      it('should have a generateCLIModeDevices function', (done) => {
        try {
          assert.equal(true, typeof a.generateCLIModeDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.generateCLIModeDevices(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateCLIModeDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceToCLIMode - errors', () => {
      it('should have a updateDeviceToCLIMode function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceToCLIMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generatevManageModeDevices - errors', () => {
      it('should have a generatevManageModeDevices function', (done) => {
        try {
          assert.equal(true, typeof a.generatevManageModeDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.generatevManageModeDevices(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generatevManageModeDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCompatibleDevices - errors', () => {
      it('should have a getCompatibleDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getCompatibleDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oldDeviceUUID', (done) => {
        try {
          a.getCompatibleDevices(null, (data, error) => {
            try {
              const displayE = 'oldDeviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCompatibleDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnForDevice - errors', () => {
      it('should have a getVpnForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getVpnForDevice(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getVpnForDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rmaUpdate - errors', () => {
      it('should have a rmaUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.rmaUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deActivatePolicy - errors', () => {
      it('should have a deActivatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deActivatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deActivatePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deActivatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePolicy - errors', () => {
      it('should have a activatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.activatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activatePolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-activatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.activatePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-activatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkVSmartConnectivityStatus - errors', () => {
      it('should have a checkVSmartConnectivityStatus function', (done) => {
        try {
          assert.equal(true, typeof a.checkVSmartConnectivityStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getColoGroups - errors', () => {
      it('should have a getColoGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getColoGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createColoGroup - errors', () => {
      it('should have a createColoGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createColoGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editColoGroup - errors', () => {
      it('should have a editColoGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editColoGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.editColoGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editColoGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteColoGroup - errors', () => {
      it('should have a deleteColoGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteColoGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteColoGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteColoGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUsers - errors', () => {
      it('should have a findUsers function', (done) => {
        try {
          assert.equal(true, typeof a.findUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUser - errors', () => {
      it('should have a createUser function', (done) => {
        try {
          assert.equal(true, typeof a.createUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSessions - errors', () => {
      it('should have a getActiveSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validatePassword - errors', () => {
      it('should have a validatePassword function', (done) => {
        try {
          assert.equal(true, typeof a.validatePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePassword - errors', () => {
      it('should have a updatePassword function', (done) => {
        try {
          assert.equal(true, typeof a.updatePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.updatePassword(null, null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-updatePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProfilePassword - errors', () => {
      it('should have a updateProfilePassword function', (done) => {
        try {
          assert.equal(true, typeof a.updateProfilePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeSessions - errors', () => {
      it('should have a removeSessions function', (done) => {
        try {
          assert.equal(true, typeof a.removeSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetUser - errors', () => {
      it('should have a resetUser function', (done) => {
        try {
          assert.equal(true, typeof a.resetUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserRole - errors', () => {
      it('should have a findUserRole function', (done) => {
        try {
          assert.equal(true, typeof a.findUserRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserAuthType - errors', () => {
      it('should have a findUserAuthType function', (done) => {
        try {
          assert.equal(true, typeof a.findUserAuthType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.updateUser(null, null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.deleteUser(null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserGroups - errors', () => {
      it('should have a findUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.findUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUserGroup - errors', () => {
      it('should have a createUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupGridColumns - errors', () => {
      it('should have a createGroupGridColumns function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupGridColumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserGroupsAsKeyValue - errors', () => {
      it('should have a findUserGroupsAsKeyValue function', (done) => {
        try {
          assert.equal(true, typeof a.findUserGroupsAsKeyValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserGroup - errors', () => {
      it('should have a updateUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userGroupId', (done) => {
        try {
          a.updateUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'userGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-updateUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserGroup - errors', () => {
      it('should have a deleteUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userGroupId', (done) => {
        try {
          a.deleteUserGroup(null, (data, error) => {
            try {
              const displayE = 'userGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnGroups - errors', () => {
      it('should have a getVpnGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnGroup - errors', () => {
      it('should have a createVpnGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVpnGroup - errors', () => {
      it('should have a editVpnGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editVpnGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.editVpnGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editVpnGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnGroup - errors', () => {
      it('should have a deleteVpnGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVpnGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteVpnGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarms - errors', () => {
      it('should have a getAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRawAlarmData - errors', () => {
      it('should have a getRawAlarmData function', (done) => {
        try {
          assert.equal(true, typeof a.getRawAlarmData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmAggregationData - errors', () => {
      it('should have a getAlarmAggregationData function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmAggregationData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostAlarmAggregationData - errors', () => {
      it('should have a getPostAlarmAggregationData function', (done) => {
        try {
          assert.equal(true, typeof a.getPostAlarmAggregationData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearStaleAlarm - errors', () => {
      it('should have a clearStaleAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.clearStaleAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNonViewedActiveAlarmsCount - errors', () => {
      it('should have a getNonViewedActiveAlarmsCount function', (done) => {
        try {
          assert.equal(true, typeof a.getNonViewedActiveAlarmsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDisabledAlarm - errors', () => {
      it('should have a listDisabledAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.listDisabledAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableEnableAlarm - errors', () => {
      it('should have a disableEnableAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.disableEnableAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventName', (done) => {
        try {
          a.disableEnableAlarm(null, null, null, null, (data, error) => {
            try {
              const displayE = 'eventName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disableEnableAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing disable', (done) => {
        try {
          a.disableEnableAlarm('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'disable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disableEnableAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing time', (done) => {
        try {
          a.disableEnableAlarm('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'time is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disableEnableAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCount1 - errors', () => {
      it('should have a getCount1 function', (done) => {
        try {
          assert.equal(true, typeof a.getCount1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getCount1(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCount1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountPost1 - errors', () => {
      it('should have a getCountPost1 function', (done) => {
        try {
          assert.equal(true, typeof a.getCountPost1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFields1 - errors', () => {
      it('should have a getStatDataFields1 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFields1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markAllAlarmsAsViewed - errors', () => {
      it('should have a markAllAlarmsAsViewed function', (done) => {
        try {
          assert.equal(true, typeof a.markAllAlarmsAsViewed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markAlarmsAsViewed - errors', () => {
      it('should have a markAlarmsAsViewed function', (done) => {
        try {
          assert.equal(true, typeof a.markAlarmsAsViewed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNonViewedAlarms - errors', () => {
      it('should have a getNonViewedAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getNonViewedAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatBulkAlarmRawData - errors', () => {
      it('should have a getStatBulkAlarmRawData function', (done) => {
        try {
          assert.equal(true, typeof a.getStatBulkAlarmRawData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getStatBulkAlarmRawData(null, null, null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getStatBulkAlarmRawData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scrollId', (done) => {
        try {
          a.getStatBulkAlarmRawData('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'scrollId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getStatBulkAlarmRawData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.getStatBulkAlarmRawData('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getStatBulkAlarmRawData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatBulkAlarmRawData - errors', () => {
      it('should have a getPostStatBulkAlarmRawData function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStatBulkAlarmRawData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scrollId', (done) => {
        try {
          a.getPostStatBulkAlarmRawData(null, null, null, (data, error) => {
            try {
              const displayE = 'scrollId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPostStatBulkAlarmRawData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.getPostStatBulkAlarmRawData('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPostStatBulkAlarmRawData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPeriodicPurgeTimer - errors', () => {
      it('should have a setPeriodicPurgeTimer function', (done) => {
        try {
          assert.equal(true, typeof a.setPeriodicPurgeTimer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatQueryFields1 - errors', () => {
      it('should have a getStatQueryFields1 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatQueryFields1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlarmQueryConfig - errors', () => {
      it('should have a createAlarmQueryConfig function', (done) => {
        try {
          assert.equal(true, typeof a.createAlarmQueryConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#correlAntiEntropy - errors', () => {
      it('should have a correlAntiEntropy function', (done) => {
        try {
          assert.equal(true, typeof a.correlAntiEntropy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restartCorrelationEngine - errors', () => {
      it('should have a restartCorrelationEngine function', (done) => {
        try {
          assert.equal(true, typeof a.restartCorrelationEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmTypesAsKeyValue - errors', () => {
      it('should have a getAlarmTypesAsKeyValue function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmTypesAsKeyValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsBySeverity - errors', () => {
      it('should have a getAlarmsBySeverity function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmsBySeverity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing severityLevel', (done) => {
        try {
          a.getAlarmsBySeverity(null, null, null, (data, error) => {
            try {
              const displayE = 'severityLevel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAlarmsBySeverity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getAlarmsBySeverity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAlarmsBySeverity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getAlarmsBySeverity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAlarmsBySeverity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSeverityCustomHistogram - errors', () => {
      it('should have a getAlarmSeverityCustomHistogram function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmSeverityCustomHistogram === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getAlarmSeverityCustomHistogram(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAlarmSeverityCustomHistogram', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSeverityMappings - errors', () => {
      it('should have a getAlarmSeverityMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmSeverityMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startTracking - errors', () => {
      it('should have a startTracking function', (done) => {
        try {
          assert.equal(true, typeof a.startTracking === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testName', (done) => {
        try {
          a.startTracking(null, (data, error) => {
            try {
              const displayE = 'testName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-startTracking', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStats - errors', () => {
      it('should have a getStats function', (done) => {
        try {
          assert.equal(true, typeof a.getStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopTracking - errors', () => {
      it('should have a stopTracking function', (done) => {
        try {
          assert.equal(true, typeof a.stopTracking === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testName', (done) => {
        try {
          a.stopTracking(null, (data, error) => {
            try {
              const displayE = 'testName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-stopTracking', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmDetails - errors', () => {
      it('should have a getAlarmDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmUuid', (done) => {
        try {
          a.getAlarmDetails(null, (data, error) => {
            try {
              const displayE = 'alarmUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAlarmDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNotificationRule - errors', () => {
      it('should have a updateNotificationRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateNotificationRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateNotificationRule(null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-updateNotificationRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNotificationRule - errors', () => {
      it('should have a createNotificationRule function', (done) => {
        try {
          assert.equal(true, typeof a.createNotificationRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotificationRule - errors', () => {
      it('should have a getNotificationRule function', (done) => {
        try {
          assert.equal(true, typeof a.getNotificationRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNotificationRule - errors', () => {
      it('should have a deleteNotificationRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNotificationRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteNotificationRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteNotificationRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawAuditLogData - errors', () => {
      it('should have a getStatDataRawAuditLogData function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawAuditLogData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRawPropertyData - errors', () => {
      it('should have a getRawPropertyData function', (done) => {
        try {
          assert.equal(true, typeof a.getRawPropertyData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getRawPropertyData(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getRawPropertyData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPropertyAggregationData - errors', () => {
      it('should have a getPropertyAggregationData function', (done) => {
        try {
          assert.equal(true, typeof a.getPropertyAggregationData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostPropertyAggregationData - errors', () => {
      it('should have a getPostPropertyAggregationData function', (done) => {
        try {
          assert.equal(true, typeof a.getPostPropertyAggregationData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getPostPropertyAggregationData(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPostPropertyAggregationData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditCount - errors', () => {
      it('should have a getAuditCount function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getAuditCount(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAuditCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditCountPost - errors', () => {
      it('should have a getAuditCountPost function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditCountPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFields - errors', () => {
      it('should have a getStatDataFields function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatBulkRawPropertyData - errors', () => {
      it('should have a getStatBulkRawPropertyData function', (done) => {
        try {
          assert.equal(true, typeof a.getStatBulkRawPropertyData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatBulkRawPropertyData - errors', () => {
      it('should have a getPostStatBulkRawPropertyData function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStatBulkRawPropertyData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getPostStatBulkRawPropertyData('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPostStatBulkRawPropertyData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatQueryFields - errors', () => {
      it('should have a getStatQueryFields function', (done) => {
        try {
          assert.equal(true, typeof a.getStatQueryFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAuditLog - errors', () => {
      it('should have a generateAuditLog function', (done) => {
        try {
          assert.equal(true, typeof a.generateAuditLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditSeverityCustomHistogram - errors', () => {
      it('should have a getAuditSeverityCustomHistogram function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditSeverityCustomHistogram === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchduledBackup - errors', () => {
      it('should have a deleteSchduledBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchduledBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalBackupInfo - errors', () => {
      it('should have a getLocalBackupInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getLocalBackupInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localBackupInfoId', (done) => {
        try {
          a.getLocalBackupInfo(null, (data, error) => {
            try {
              const displayE = 'localBackupInfoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getLocalBackupInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadBackupFile - errors', () => {
      it('should have a downloadBackupFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadBackupFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.downloadBackupFile(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadBackupFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBackup - errors', () => {
      it('should have a listBackup function', (done) => {
        try {
          assert.equal(true, typeof a.listBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importScheduledBackup - errors', () => {
      it('should have a importScheduledBackup function', (done) => {
        try {
          assert.equal(true, typeof a.importScheduledBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteImportBackup - errors', () => {
      it('should have a remoteImportBackup function', (done) => {
        try {
          assert.equal(true, typeof a.remoteImportBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSchedules - errors', () => {
      it('should have a listSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.listSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleRecordForBackup - errors', () => {
      it('should have a getScheduleRecordForBackup function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleRecordForBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schedulerId', (done) => {
        try {
          a.getScheduleRecordForBackup(null, (data, error) => {
            try {
              const displayE = 'schedulerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getScheduleRecordForBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedule - errors', () => {
      it('should have a deleteSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schedulerId', (done) => {
        try {
          a.deleteSchedule(null, (data, error) => {
            try {
              const displayE = 'schedulerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportBackup - errors', () => {
      it('should have a exportBackup function', (done) => {
        try {
          assert.equal(true, typeof a.exportBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertDetails - errors', () => {
      it('should have a getCertDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getCertDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCSRViewRightMenus - errors', () => {
      it('should have a getCSRViewRightMenus function', (done) => {
        try {
          assert.equal(true, typeof a.getCSRViewRightMenus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceViewRightMenus - errors', () => {
      it('should have a getDeviceViewRightMenus function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceViewRightMenus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesList - errors', () => {
      it('should have a getDevicesList function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forceSyncRootCert - errors', () => {
      it('should have a forceSyncRootCert function', (done) => {
        try {
          assert.equal(true, typeof a.forceSyncRootCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateCSR - errors', () => {
      it('should have a generateCSR function', (done) => {
        try {
          assert.equal(true, typeof a.generateCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateEnterpriseCSR - errors', () => {
      it('should have a generateEnterpriseCSR function', (done) => {
        try {
          assert.equal(true, typeof a.generateEnterpriseCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installCertificate - errors', () => {
      it('should have a installCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.installCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListStatus - errors', () => {
      it('should have a getListStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getListStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateData - errors', () => {
      it('should have a getCertificateData function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetRSA - errors', () => {
      it('should have a resetRSA function', (done) => {
        try {
          assert.equal(true, typeof a.resetRSA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decommissionEnterpriseCSRForVedge - errors', () => {
      it('should have a decommissionEnterpriseCSRForVedge function', (done) => {
        try {
          assert.equal(true, typeof a.decommissionEnterpriseCSRForVedge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootCertChains - errors', () => {
      it('should have a getRootCertChains function', (done) => {
        try {
          assert.equal(true, typeof a.getRootCertChains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.getRootCertChains(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getRootCertChains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveRootCertChain - errors', () => {
      it('should have a saveRootCertChain function', (done) => {
        try {
          assert.equal(true, typeof a.saveRootCertChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootCertificate - errors', () => {
      it('should have a getRootCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getRootCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveVEdgeList - errors', () => {
      it('should have a saveVEdgeList function', (done) => {
        try {
          assert.equal(true, typeof a.saveVEdgeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateDetail - errors', () => {
      it('should have a getCertificateDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateStats - errors', () => {
      it('should have a getCertificateStats function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#syncvBond - errors', () => {
      it('should have a syncvBond function', (done) => {
        try {
          assert.equal(true, typeof a.syncvBond === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvEdgeList - errors', () => {
      it('should have a getvEdgeList function', (done) => {
        try {
          assert.equal(true, typeof a.getvEdgeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setvEdgeList - errors', () => {
      it('should have a setvEdgeList function', (done) => {
        try {
          assert.equal(true, typeof a.setvEdgeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getView - errors', () => {
      it('should have a getView function', (done) => {
        try {
          assert.equal(true, typeof a.getView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvSmartList - errors', () => {
      it('should have a getvSmartList function', (done) => {
        try {
          assert.equal(true, typeof a.getvSmartList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setvSmartList - errors', () => {
      it('should have a setvSmartList function', (done) => {
        try {
          assert.equal(true, typeof a.setvSmartList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfiguration - errors', () => {
      it('should have a deleteConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSelfSignedCert - errors', () => {
      it('should have a getSelfSignedCert function', (done) => {
        try {
          assert.equal(true, typeof a.getSelfSignedCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyCertOfEdge - errors', () => {
      it('should have a getProxyCertOfEdge function', (done) => {
        try {
          assert.equal(true, typeof a.getProxyCertOfEdge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getProxyCertOfEdge(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getProxyCertOfEdge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCertificate - errors', () => {
      it('should have a updateCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.updateCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addWANEdge - errors', () => {
      it('should have a addWANEdge function', (done) => {
        try {
          assert.equal(true, typeof a.addWANEdge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.addWANEdge(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-addWANEdge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCertificiates - errors', () => {
      it('should have a uploadCertificiates function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCertificiates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSslProxyCSR - errors', () => {
      it('should have a getSslProxyCSR function', (done) => {
        try {
          assert.equal(true, typeof a.getSslProxyCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getSslProxyCSR(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getSslProxyCSR', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceCertificates - errors', () => {
      it('should have a getAllDeviceCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceCSR - errors', () => {
      it('should have a getAllDeviceCSR function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateSslProxyCSR - errors', () => {
      it('should have a generateSslProxyCSR function', (done) => {
        try {
          assert.equal(true, typeof a.generateSslProxyCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateSSLProxyCSR - errors', () => {
      it('should have a generateSSLProxyCSR function', (done) => {
        try {
          assert.equal(true, typeof a.generateSSLProxyCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSslProxyList - errors', () => {
      it('should have a getSslProxyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSslProxyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewCertificate - errors', () => {
      it('should have a renewCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.renewCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCertificate - errors', () => {
      it('should have a revokeCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeRenewCertificate - errors', () => {
      it('should have a revokeRenewCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.revokeRenewCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateState - errors', () => {
      it('should have a getCertificateState function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnterpriseCertificate - errors', () => {
      it('should have a getEnterpriseCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getEnterpriseCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setEnterpriseCert - errors', () => {
      it('should have a setEnterpriseCert function', (done) => {
        try {
          assert.equal(true, typeof a.setEnterpriseCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVManageEnterpriseRootCertificate - errors', () => {
      it('should have a getVManageEnterpriseRootCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getVManageEnterpriseRootCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setEnterpriseRootCaCert - errors', () => {
      it('should have a setEnterpriseRootCaCert function', (done) => {
        try {
          assert.equal(true, typeof a.setEnterpriseRootCaCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvManageCertificate - errors', () => {
      it('should have a getvManageCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getvManageCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setvManageintermediateCert - errors', () => {
      it('should have a setvManageintermediateCert function', (done) => {
        try {
          assert.equal(true, typeof a.setvManageintermediateCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvManageCSR - errors', () => {
      it('should have a getvManageCSR function', (done) => {
        try {
          assert.equal(true, typeof a.getvManageCSR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvManageRootCA - errors', () => {
      it('should have a getvManageRootCA function', (done) => {
        try {
          assert.equal(true, typeof a.getvManageRootCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setvManageRootCA - errors', () => {
      it('should have a setvManageRootCA function', (done) => {
        try {
          assert.equal(true, typeof a.setvManageRootCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessTokenforDevice - errors', () => {
      it('should have a getAccessTokenforDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessTokenforDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureToken - errors', () => {
      it('should have a getAzureToken function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemconnect - errors', () => {
      it('should have a systemconnect function', (done) => {
        try {
          assert.equal(true, typeof a.systemconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudCredentials - errors', () => {
      it('should have a getCloudCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCloudCredentials - errors', () => {
      it('should have a addCloudCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.addCloudCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCode - errors', () => {
      it('should have a getDeviceCode function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#isStaging - errors', () => {
      it('should have a isStaging function', (done) => {
        try {
          assert.equal(true, typeof a.isStaging === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelemetryState - errors', () => {
      it('should have a getTelemetryState function', (done) => {
        try {
          assert.equal(true, typeof a.getTelemetryState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#optIn - errors', () => {
      it('should have a optIn function', (done) => {
        try {
          assert.equal(true, typeof a.optIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#optOut - errors', () => {
      it('should have a optOut function', (done) => {
        try {
          assert.equal(true, typeof a.optOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudSettings - errors', () => {
      it('should have a getCloudSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOTP - errors', () => {
      it('should have a getOTP function', (done) => {
        try {
          assert.equal(true, typeof a.getOTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatetOTP - errors', () => {
      it('should have a updatetOTP function', (done) => {
        try {
          assert.equal(true, typeof a.updatetOTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEntityOwnershipInfo - errors', () => {
      it('should have a listEntityOwnershipInfo function', (done) => {
        try {
          assert.equal(true, typeof a.listEntityOwnershipInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#entityOwnershipInfo - errors', () => {
      it('should have a entityOwnershipInfo function', (done) => {
        try {
          assert.equal(true, typeof a.entityOwnershipInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkIfClusterLocked - errors', () => {
      it('should have a checkIfClusterLocked function', (done) => {
        try {
          assert.equal(true, typeof a.checkIfClusterLocked === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configureVmanage - errors', () => {
      it('should have a configureVmanage function', (done) => {
        try {
          assert.equal(true, typeof a.configureVmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectedDevices - errors', () => {
      it('should have a getConnectedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmanageIP', (done) => {
        try {
          a.getConnectedDevices(null, (data, error) => {
            try {
              const displayE = 'vmanageIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getConnectedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableSDAVC - errors', () => {
      it('should have a enableSDAVC function', (done) => {
        try {
          assert.equal(true, typeof a.enableSDAVC === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthDetails - errors', () => {
      it('should have a healthDetails function', (done) => {
        try {
          assert.equal(true, typeof a.healthDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthStatusInfo - errors', () => {
      it('should have a healthStatusInfo function', (done) => {
        try {
          assert.equal(true, typeof a.healthStatusInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthSummary - errors', () => {
      it('should have a healthSummary function', (done) => {
        try {
          assert.equal(true, typeof a.healthSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfiguredIPList - errors', () => {
      it('should have a getConfiguredIPList function', (done) => {
        try {
          assert.equal(true, typeof a.getConfiguredIPList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmanageID', (done) => {
        try {
          a.getConfiguredIPList(null, (data, error) => {
            try {
              const displayE = 'vmanageID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getConfiguredIPList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#isClusterReady - errors', () => {
      it('should have a isClusterReady function', (done) => {
        try {
          assert.equal(true, typeof a.isClusterReady === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVmanages - errors', () => {
      it('should have a listVmanages function', (done) => {
        try {
          assert.equal(true, typeof a.listVmanages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#nodeProperties - errors', () => {
      it('should have a nodeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.nodeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeVmanage - errors', () => {
      it('should have a removeVmanage function', (done) => {
        try {
          assert.equal(true, typeof a.removeVmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#performReplicationAndRebalanceOfKafkaPartitions - errors', () => {
      it('should have a performReplicationAndRebalanceOfKafkaPartitions function', (done) => {
        try {
          assert.equal(true, typeof a.performReplicationAndRebalanceOfKafkaPartitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetSDAVCNode - errors', () => {
      it('should have a resetSDAVCNode function', (done) => {
        try {
          assert.equal(true, typeof a.resetSDAVCNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVmanage - errors', () => {
      it('should have a editVmanage function', (done) => {
        try {
          assert.equal(true, typeof a.editVmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVmanage - errors', () => {
      it('should have a addVmanage function', (done) => {
        try {
          assert.equal(true, typeof a.addVmanage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyMode - errors', () => {
      it('should have a getTenancyMode function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setTenancyMode - errors', () => {
      it('should have a setTenancyMode function', (done) => {
        try {
          assert.equal(true, typeof a.setTenancyMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantsList - errors', () => {
      it('should have a getTenantsList function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReplicationFactorForKafkaTopics - errors', () => {
      it('should have a updateReplicationFactorForKafkaTopics function', (done) => {
        try {
          assert.equal(true, typeof a.updateReplicationFactorForKafkaTopics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOrUpdateUserCredentials - errors', () => {
      it('should have a addOrUpdateUserCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.addOrUpdateUserCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVManageDetails - errors', () => {
      it('should have a getVManageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getVManageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmanageIP', (done) => {
        try {
          a.getVManageDetails(null, (data, error) => {
            try {
              const displayE = 'vmanageIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getVManageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectedDevicesPerTenant - errors', () => {
      it('should have a getConnectedDevicesPerTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectedDevicesPerTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getConnectedDevicesPerTenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getConnectedDevicesPerTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmanageIP', (done) => {
        try {
          a.getConnectedDevicesPerTenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vmanageIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getConnectedDevicesPerTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudDockClusterDetail - errors', () => {
      it('should have a getCloudDockClusterDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudDockClusterDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.getCloudDockClusterDetail(null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCloudDockClusterDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCloudDockCluster - errors', () => {
      it('should have a updateCloudDockCluster function', (done) => {
        try {
          assert.equal(true, typeof a.updateCloudDockCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCloudDockCluster - errors', () => {
      it('should have a createCloudDockCluster function', (done) => {
        try {
          assert.equal(true, typeof a.createCloudDockCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acitvateCloudDockCluster - errors', () => {
      it('should have a acitvateCloudDockCluster function', (done) => {
        try {
          assert.equal(true, typeof a.acitvateCloudDockCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.acitvateCloudDockCluster(null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-acitvateCloudDockCluster', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dummyccm - errors', () => {
      it('should have a dummyccm function', (done) => {
        try {
          assert.equal(true, typeof a.dummyccm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.dummyccm(null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-dummyccm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dummycspState - errors', () => {
      it('should have a dummycspState function', (done) => {
        try {
          assert.equal(true, typeof a.dummycspState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.dummycspState(null, null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-dummycspState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.dummycspState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-dummycspState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCspToCluster - errors', () => {
      it('should have a updateCspToCluster function', (done) => {
        try {
          assert.equal(true, typeof a.updateCspToCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloudDockClusterPreview - errors', () => {
      it('should have a cloudDockClusterPreview function', (done) => {
        try {
          assert.equal(true, typeof a.cloudDockClusterPreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.cloudDockClusterPreview(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-cloudDockClusterPreview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deAcitvateCloudDockCluster - errors', () => {
      it('should have a deAcitvateCloudDockCluster function', (done) => {
        try {
          assert.equal(true, typeof a.deAcitvateCloudDockCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.deAcitvateCloudDockCluster(null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deAcitvateCloudDockCluster', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudDockClusterDetailById - errors', () => {
      it('should have a getCloudDockClusterDetailById function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudDockClusterDetailById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.getCloudDockClusterDetailById(null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCloudDockClusterDetailById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudDockClusterByName - errors', () => {
      it('should have a deleteCloudDockClusterByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCloudDockClusterByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clustername', (done) => {
        try {
          a.deleteCloudDockClusterByName(null, (data, error) => {
            try {
              const displayE = 'clustername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteCloudDockClusterByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterDetailsByClusterId - errors', () => {
      it('should have a getClusterDetailsByClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterDetailsByClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.getClusterDetailsByClusterId(null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getClusterDetailsByClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterConfigByClusterId - errors', () => {
      it('should have a getClusterConfigByClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterConfigByClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.getClusterConfigByClusterId(null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getClusterConfigByClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterPortMappingByClusterId - errors', () => {
      it('should have a getClusterPortMappingByClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterPortMappingByClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.getClusterPortMappingByClusterId(null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getClusterPortMappingByClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDetailByDeviceId - errors', () => {
      it('should have a getDeviceDetailByDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDetailByDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemStatusByDeviceId - errors', () => {
      it('should have a getSystemStatusByDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemStatusByDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvnfByDeviceId - errors', () => {
      it('should have a getvnfByDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getvnfByDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkFunctionMap - errors', () => {
      it('should have a listNetworkFunctionMap function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkFunctionMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpnfDetails - errors', () => {
      it('should have a getpnfDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getpnfDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPNFConfig - errors', () => {
      it('should have a getPNFConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getPNFConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceChainDetails - errors', () => {
      it('should have a getServiceChainDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceChainDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceGroupByClusterId - errors', () => {
      it('should have a getServiceGroupByClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceGroupByClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getvnfDetails - errors', () => {
      it('should have a getvnfDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getvnfDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vnfActions - errors', () => {
      it('should have a vnfActions function', (done) => {
        try {
          assert.equal(true, typeof a.vnfActions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVNFEventsCountDetail - errors', () => {
      it('should have a getVNFEventsCountDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getVNFEventsCountDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVNFAlarmCount - errors', () => {
      it('should have a getVNFAlarmCount function', (done) => {
        try {
          assert.equal(true, typeof a.getVNFAlarmCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVNFEventsDetail - errors', () => {
      it('should have a getVNFEventsDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getVNFEventsDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVNFInterfaceDetail - errors', () => {
      it('should have a getVNFInterfaceDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getVNFInterfaceDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachServiceChain - errors', () => {
      it('should have a attachServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.attachServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachServiceChain - errors', () => {
      it('should have a detachServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.detachServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEdgeDevices - errors', () => {
      it('should have a getEdgeDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getEdgeDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpnfDevices - errors', () => {
      it('should have a getpnfDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getpnfDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pnfDeviceType', (done) => {
        try {
          a.getpnfDevices(null, (data, error) => {
            try {
              const displayE = 'pnfDeviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getpnfDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceChain - errors', () => {
      it('should have a getServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceGroupName', (done) => {
        try {
          a.getServiceChain(null, (data, error) => {
            try {
              const displayE = 'serviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServiceGroupCluster - errors', () => {
      it('should have a updateServiceGroupCluster function', (done) => {
        try {
          assert.equal(true, typeof a.updateServiceGroupCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServiceGroupCluster - errors', () => {
      it('should have a createServiceGroupCluster function', (done) => {
        try {
          assert.equal(true, typeof a.createServiceGroupCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceGroupInCluster - errors', () => {
      it('should have a getServiceGroupInCluster function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceGroupInCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultChain - errors', () => {
      it('should have a getDefaultChain function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableChains - errors', () => {
      it('should have a getAvailableChains function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailableChains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceGroupCluster - errors', () => {
      it('should have a deleteServiceGroupCluster function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceGroupCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteServiceGroupCluster(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteServiceGroupCluster', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateContainerOnRemoteHost - errors', () => {
      it('should have a activateContainerOnRemoteHost function', (done) => {
        try {
          assert.equal(true, typeof a.activateContainerOnRemoteHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerName', (done) => {
        try {
          a.activateContainerOnRemoteHost(null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-activateContainerOnRemoteHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deActivateContainer - errors', () => {
      it('should have a deActivateContainer function', (done) => {
        try {
          assert.equal(true, typeof a.deActivateContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerName', (done) => {
        try {
          a.deActivateContainer(null, null, (data, error) => {
            try {
              const displayE = 'containerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deActivateContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#doesValidImageExist - errors', () => {
      it('should have a doesValidImageExist function', (done) => {
        try {
          assert.equal(true, typeof a.doesValidImageExist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerName', (done) => {
        try {
          a.doesValidImageExist(null, (data, error) => {
            try {
              const displayE = 'containerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-doesValidImageExist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainerInspectData - errors', () => {
      it('should have a getContainerInspectData function', (done) => {
        try {
          assert.equal(true, typeof a.getContainerInspectData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerName', (done) => {
        try {
          a.getContainerInspectData(null, null, (data, error) => {
            try {
              const displayE = 'containerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getContainerInspectData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainerSettings - errors', () => {
      it('should have a getContainerSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getContainerSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerName', (done) => {
        try {
          a.getContainerSettings(null, null, (data, error) => {
            try {
              const displayE = 'containerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getContainerSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChecksum - errors', () => {
      it('should have a getChecksum function', (done) => {
        try {
          assert.equal(true, typeof a.getChecksum === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomApp - errors', () => {
      it('should have a getCustomApp function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateContainer - errors', () => {
      it('should have a activateContainer function', (done) => {
        try {
          assert.equal(true, typeof a.activateContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.activateContainer(null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-activateContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testLoadBalancer - errors', () => {
      it('should have a testLoadBalancer function', (done) => {
        try {
          assert.equal(true, typeof a.testLoadBalancer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setBlockSync - errors', () => {
      it('should have a setBlockSync function', (done) => {
        try {
          assert.equal(true, typeof a.setBlockSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockSync', (done) => {
        try {
          a.setBlockSync(null, (data, error) => {
            try {
              const displayE = 'blockSync is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-setBlockSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableSDAVCOnDevice - errors', () => {
      it('should have a enableSDAVCOnDevice function', (done) => {
        try {
          assert.equal(true, typeof a.enableSDAVCOnDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.enableSDAVCOnDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-enableSDAVCOnDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enable', (done) => {
        try {
          a.enableSDAVCOnDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'enable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-enableSDAVCOnDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsType - errors', () => {
      it('should have a getStatisticsType function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlarms - errors', () => {
      it('should have a getActiveAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scrollId', (done) => {
        try {
          a.getActiveAlarms(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'scrollId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getActiveAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.getActiveAlarms('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getActiveAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.getActiveAlarms('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getActiveAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.getActiveAlarms('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getActiveAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeZone', (done) => {
        try {
          a.getActiveAlarms('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'timeZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getActiveAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDeviceStatisticsData - errors', () => {
      it('should have a generateDeviceStatisticsData function', (done) => {
        try {
          assert.equal(true, typeof a.generateDeviceStatisticsData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.generateDeviceStatisticsData(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scrollId', (done) => {
        try {
          a.generateDeviceStatisticsData('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'scrollId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.generateDeviceStatisticsData('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.generateDeviceStatisticsData('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeZone', (done) => {
        try {
          a.generateDeviceStatisticsData('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'timeZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountWithStateDataType - errors', () => {
      it('should have a getCountWithStateDataType function', (done) => {
        try {
          assert.equal(true, typeof a.getCountWithStateDataType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.getCountWithStateDataType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCountWithStateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.getCountWithStateDataType('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCountWithStateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.getCountWithStateDataType('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCountWithStateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeZone', (done) => {
        try {
          a.getCountWithStateDataType('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'timeZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCountWithStateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFieldsByStateDataType - errors', () => {
      it('should have a getStatDataFieldsByStateDataType function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFieldsByStateDataType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.getStatDataFieldsByStateDataType(null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getStatDataFieldsByStateDataType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStats - errors', () => {
      it('should have a createStats function', (done) => {
        try {
          assert.equal(true, typeof a.createStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllStatsDataDCA - errors', () => {
      it('should have a getAllStatsDataDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getAllStatsDataDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessToken - errors', () => {
      it('should have a getAccessToken function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#storeAccessToken - errors', () => {
      it('should have a storeAccessToken function', (done) => {
        try {
          assert.equal(true, typeof a.storeAccessToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAlarm - errors', () => {
      it('should have a generateAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.generateAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdToken - errors', () => {
      it('should have a getIdToken function', (done) => {
        try {
          assert.equal(true, typeof a.getIdToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#storeIdToken - errors', () => {
      it('should have a storeIdToken function', (done) => {
        try {
          assert.equal(true, typeof a.storeIdToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelemetrySettings - errors', () => {
      it('should have a getTelemetrySettings function', (done) => {
        try {
          assert.equal(true, typeof a.getTelemetrySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDCADeviceStateData - errors', () => {
      it('should have a generateDCADeviceStateData function', (done) => {
        try {
          assert.equal(true, typeof a.generateDCADeviceStateData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateDataType', (done) => {
        try {
          a.generateDCADeviceStateData(null, null, (data, error) => {
            try {
              const displayE = 'stateDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDCADeviceStateData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDCADeviceStatisticsData - errors', () => {
      it('should have a generateDCADeviceStatisticsData function', (done) => {
        try {
          assert.equal(true, typeof a.generateDCADeviceStatisticsData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statsDataType', (done) => {
        try {
          a.generateDCADeviceStatisticsData(null, null, (data, error) => {
            try {
              const displayE = 'statsDataType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateDCADeviceStatisticsData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDCATenantOwners - errors', () => {
      it('should have a getDCATenantOwners function', (done) => {
        try {
          assert.equal(true, typeof a.getDCATenantOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllDevicesDCA - errors', () => {
      it('should have a listAllDevicesDCA function', (done) => {
        try {
          assert.equal(true, typeof a.listAllDevicesDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrashLogs - errors', () => {
      it('should have a getCrashLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getCrashLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrashLogsSynced - errors', () => {
      it('should have a getCrashLogsSynced function', (done) => {
        try {
          assert.equal(true, typeof a.getCrashLogsSynced === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getCrashLogsSynced(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCrashLogsSynced', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudServicesConfigurationDCA - errors', () => {
      it('should have a getCloudServicesConfigurationDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudServicesConfigurationDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDCAAnalyticsDataFile - errors', () => {
      it('should have a createDCAAnalyticsDataFile function', (done) => {
        try {
          assert.equal(true, typeof a.createDCAAnalyticsDataFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.createDCAAnalyticsDataFile(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-createDCAAnalyticsDataFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsDBIndexStatus - errors', () => {
      it('should have a getStatsDBIndexStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsDBIndexStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesDetailsDCA - errors', () => {
      it('should have a getDevicesDetailsDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesDetailsDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDCAAttachedConfigToDevice - errors', () => {
      it('should have a getDCAAttachedConfigToDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDCAAttachedConfigToDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplatePolicyDefinitionsDCA - errors', () => {
      it('should have a getTemplatePolicyDefinitionsDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplatePolicyDefinitionsDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVPNListsDCA - errors', () => {
      it('should have a getVPNListsDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getVPNListsDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVedgeTemplateListDCA - errors', () => {
      it('should have a getVedgeTemplateListDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getVedgeTemplateListDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVsmartTemplateListDCA - errors', () => {
      it('should have a getVsmartTemplateListDCA function', (done) => {
        try {
          assert.equal(true, typeof a.getVsmartTemplateListDCA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAAUsers - errors', () => {
      it('should have a getAAAUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAAAUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getAAAUsers(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAAAUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getACLMatchCounterUsers - errors', () => {
      it('should have a getACLMatchCounterUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getACLMatchCounterUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getACLMatchCounterUsers(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getACLMatchCounterUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnclaimedVedges - errors', () => {
      it('should have a getUnclaimedVedges function', (done) => {
        try {
          assert.equal(true, typeof a.getUnclaimedVedges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getUnclaimedVedges(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getUnclaimedVedges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersFromDevice - errors', () => {
      it('should have a getUsersFromDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersFromDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getUsersFromDevice(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getUsersFromDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceUsers - errors', () => {
      it('should have a getAllDeviceUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getAllDeviceUsers(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getAllDeviceUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcActivate - errors', () => {
      it('should have a processLxcActivate function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcActivate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcDelete - errors', () => {
      it('should have a processLxcDelete function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcInstall - errors', () => {
      it('should have a processLxcInstall function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcInstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcReload - errors', () => {
      it('should have a processLxcReload function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcReload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcReset - errors', () => {
      it('should have a processLxcReset function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processLxcUpgrade - errors', () => {
      it('should have a processLxcUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.processLxcUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processDeleteAmpApiKey - errors', () => {
      it('should have a processDeleteAmpApiKey function', (done) => {
        try {
          assert.equal(true, typeof a.processDeleteAmpApiKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.processDeleteAmpApiKey(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processDeleteAmpApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processAmpApiReKey - errors', () => {
      it('should have a processAmpApiReKey function', (done) => {
        try {
          assert.equal(true, typeof a.processAmpApiReKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testApiKey - errors', () => {
      it('should have a testApiKey function', (done) => {
        try {
          assert.equal(true, typeof a.testApiKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.testApiKey(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-testApiKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateSecurityDevicesList - errors', () => {
      it('should have a generateSecurityDevicesList function', (done) => {
        try {
          assert.equal(true, typeof a.generateSecurityDevicesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyType', (done) => {
        try {
          a.generateSecurityDevicesList(null, null, (data, error) => {
            try {
              const displayE = 'policyType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateSecurityDevicesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.generateSecurityDevicesList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-generateSecurityDevicesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testIoxConfig - errors', () => {
      it('should have a testIoxConfig function', (done) => {
        try {
          assert.equal(true, typeof a.testIoxConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.testIoxConfig(null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-testIoxConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processVnfInstall - errors', () => {
      it('should have a processVnfInstall function', (done) => {
        try {
          assert.equal(true, typeof a.processVnfInstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSoftwareImages - errors', () => {
      it('should have a findSoftwareImages function', (done) => {
        try {
          assert.equal(true, typeof a.findSoftwareImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createImageURL - errors', () => {
      it('should have a createImageURL function', (done) => {
        try {
          assert.equal(true, typeof a.createImageURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageProperties - errors', () => {
      it('should have a getImageProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getImageProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.getImageProperties(null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getImageProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSoftwareImagesWithFilters - errors', () => {
      it('should have a findSoftwareImagesWithFilters function', (done) => {
        try {
          assert.equal(true, typeof a.findSoftwareImagesWithFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageType', (done) => {
        try {
          a.findSoftwareImagesWithFilters(null, null, (data, error) => {
            try {
              const displayE = 'imageType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-findSoftwareImagesWithFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPnfProperties - errors', () => {
      it('should have a getPnfProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getPnfProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pnfType', (done) => {
        try {
          a.getPnfProperties(null, (data, error) => {
            try {
              const displayE = 'pnfType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPnfProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findVEdgeSoftwareVersion - errors', () => {
      it('should have a findVEdgeSoftwareVersion function', (done) => {
        try {
          assert.equal(true, typeof a.findVEdgeSoftwareVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSoftwareVersion - errors', () => {
      it('should have a findSoftwareVersion function', (done) => {
        try {
          assert.equal(true, typeof a.findSoftwareVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVnfProperties - errors', () => {
      it('should have a getVnfProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getVnfProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.getVnfProperties(null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getVnfProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findZtpSoftwareVersion - errors', () => {
      it('should have a findZtpSoftwareVersion function', (done) => {
        try {
          assert.equal(true, typeof a.findZtpSoftwareVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImageURL - errors', () => {
      it('should have a updateImageURL function', (done) => {
        try {
          assert.equal(true, typeof a.updateImageURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.updateImageURL(null, null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-updateImageURL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteImageURL - errors', () => {
      it('should have a deleteImageURL function', (done) => {
        try {
          assert.equal(true, typeof a.deleteImageURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.deleteImageURL(null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteImageURL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installPkg - errors', () => {
      it('should have a installPkg function', (done) => {
        try {
          assert.equal(true, typeof a.installPkg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUploadImagesCount - errors', () => {
      it('should have a getUploadImagesCount function', (done) => {
        try {
          assert.equal(true, typeof a.getUploadImagesCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageType', (done) => {
        try {
          a.getUploadImagesCount(null, (data, error) => {
            try {
              const displayE = 'imageType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getUploadImagesCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadPackageFile - errors', () => {
      it('should have a downloadPackageFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadPackageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.downloadPackageFile(null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageType', (done) => {
        try {
          a.downloadPackageFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'imageType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processSoftwareImage - errors', () => {
      it('should have a processSoftwareImage function', (done) => {
        try {
          assert.equal(true, typeof a.processSoftwareImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageType', (done) => {
        try {
          a.processSoftwareImage(null, (data, error) => {
            try {
              const displayE = 'imageType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processSoftwareImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileContents - errors', () => {
      it('should have a getFileContents function', (done) => {
        try {
          assert.equal(true, typeof a.getFileContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getFileContents(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getFileContents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editConfigFile - errors', () => {
      it('should have a editConfigFile function', (done) => {
        try {
          assert.equal(true, typeof a.editConfigFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.editConfigFile(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-editConfigFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadImageFile - errors', () => {
      it('should have a uploadImageFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadImageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.uploadImageFile(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-uploadImageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVnfPackage - errors', () => {
      it('should have a createVnfPackage function', (done) => {
        try {
          assert.equal(true, typeof a.createVnfPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelPendingTasks - errors', () => {
      it('should have a cancelPendingTasks function', (done) => {
        try {
          assert.equal(true, typeof a.cancelPendingTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processId', (done) => {
        try {
          a.cancelPendingTasks(null, (data, error) => {
            try {
              const displayE = 'processId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-cancelPendingTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cleanStatus - errors', () => {
      it('should have a cleanStatus function', (done) => {
        try {
          assert.equal(true, typeof a.cleanStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cleanStatus', (done) => {
        try {
          a.cleanStatus(null, (data, error) => {
            try {
              const displayE = 'cleanStatus is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-cleanStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStatus - errors', () => {
      it('should have a deleteStatus function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processId', (done) => {
        try {
          a.deleteStatus(null, (data, error) => {
            try {
              const displayE = 'processId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findRunningTasks - errors', () => {
      it('should have a findRunningTasks function', (done) => {
        try {
          assert.equal(true, typeof a.findRunningTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveTaskCount - errors', () => {
      it('should have a getActiveTaskCount function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveTaskCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCleanStatus - errors', () => {
      it('should have a getCleanStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCleanStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processId', (done) => {
        try {
          a.getCleanStatus(null, (data, error) => {
            try {
              const displayE = 'processId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCleanStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeatureList - errors', () => {
      it('should have a getFeatureList function', (done) => {
        try {
          assert.equal(true, typeof a.getFeatureList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getFeatureList(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getFeatureList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyncedFeatureList - errors', () => {
      it('should have a getSyncedFeatureList function', (done) => {
        try {
          assert.equal(true, typeof a.getSyncedFeatureList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getSyncedFeatureList(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getSyncedFeatureList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawData16 - errors', () => {
      it('should have a getStatDataRawData16 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawData16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsRawData16 - errors', () => {
      it('should have a getStatsRawData16 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsRawData16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAggregationDataByQuery16 - errors', () => {
      it('should have a getAggregationDataByQuery16 function', (done) => {
        try {
          assert.equal(true, typeof a.getAggregationDataByQuery16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostAggregationDataByQuery15 - errors', () => {
      it('should have a getPostAggregationDataByQuery15 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostAggregationDataByQuery15 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLastThousandConfigList - errors', () => {
      it('should have a getLastThousandConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.getLastThousandConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getLastThousandConfigList(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getLastThousandConfigList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getLastThousandConfigList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getLastThousandConfigList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfig - errors', () => {
      it('should have a getDeviceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configId', (done) => {
        try {
          a.getDeviceConfig(null, (data, error) => {
            try {
              const displayE = 'configId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawDataAsCSV16 - errors', () => {
      it('should have a getStatDataRawDataAsCSV16 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawDataAsCSV16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCount18 - errors', () => {
      it('should have a getCount18 function', (done) => {
        try {
          assert.equal(true, typeof a.getCount18 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getCount18(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCount18', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountPost18 - errors', () => {
      it('should have a getCountPost18 function', (done) => {
        try {
          assert.equal(true, typeof a.getCountPost18 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFields18 - errors', () => {
      it('should have a getStatDataFields18 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFields18 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatBulkRawData16 - errors', () => {
      it('should have a getStatBulkRawData16 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatBulkRawData16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatBulkRawData16 - errors', () => {
      it('should have a getPostStatBulkRawData16 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStatBulkRawData16 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatQueryFields18 - errors', () => {
      it('should have a getStatQueryFields18 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatQueryFields18 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdminTech - errors', () => {
      it('should have a createAdminTech function', (done) => {
        try {
          assert.equal(true, typeof a.createAdminTech === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadAdminTechFile - errors', () => {
      it('should have a downloadAdminTechFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadAdminTechFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.downloadAdminTechFile(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadAdminTechFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdminTechFile - errors', () => {
      it('should have a deleteAdminTechFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdminTechFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestID', (done) => {
        try {
          a.deleteAdminTechFile(null, (data, error) => {
            try {
              const displayE = 'requestID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-deleteAdminTechFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdminTechs - errors', () => {
      it('should have a listAdminTechs function', (done) => {
        try {
          assert.equal(true, typeof a.listAdminTechs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#npingDevice - errors', () => {
      it('should have a npingDevice function', (done) => {
        try {
          assert.equal(true, typeof a.npingDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.npingDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-npingDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pingDevice - errors', () => {
      it('should have a pingDevice function', (done) => {
        try {
          assert.equal(true, typeof a.pingDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.pingDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-pingDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processPortHopColor - errors', () => {
      it('should have a processPortHopColor function', (done) => {
        try {
          assert.equal(true, typeof a.processPortHopColor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.processPortHopColor(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processPortHopColor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processInterfaceReset - errors', () => {
      it('should have a processInterfaceReset function', (done) => {
        try {
          assert.equal(true, typeof a.processInterfaceReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.processInterfaceReset(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processInterfaceReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processResetUser - errors', () => {
      it('should have a processResetUser function', (done) => {
        try {
          assert.equal(true, typeof a.processResetUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.processResetUser(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processResetUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#servicePath - errors', () => {
      it('should have a servicePath function', (done) => {
        try {
          assert.equal(true, typeof a.servicePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.servicePath(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-servicePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tracerouteDevice - errors', () => {
      it('should have a tracerouteDevice function', (done) => {
        try {
          assert.equal(true, typeof a.tracerouteDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.tracerouteDevice(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-tracerouteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelPath - errors', () => {
      it('should have a tunnelPath function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIP', (done) => {
        try {
          a.tunnelPath(null, null, (data, error) => {
            try {
              const displayE = 'deviceIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-tunnelPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControlConnections - errors', () => {
      it('should have a getControlConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getControlConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getControlConnections(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getControlConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfiguration - errors', () => {
      it('should have a getDeviceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getDeviceConfiguration(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDBSchema - errors', () => {
      it('should have a getDBSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getDBSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThreadPools - errors', () => {
      it('should have a getThreadPools function', (done) => {
        try {
          assert.equal(true, typeof a.getThreadPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawData23 - errors', () => {
      it('should have a getStatDataRawData23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawData23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsRawData23 - errors', () => {
      it('should have a getStatsRawData23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsRawData23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAggregationDataByQuery23 - errors', () => {
      it('should have a getAggregationDataByQuery23 function', (done) => {
        try {
          assert.equal(true, typeof a.getAggregationDataByQuery23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostAggregationDataByQuery22 - errors', () => {
      it('should have a getPostAggregationDataByQuery22 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostAggregationDataByQuery22 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawDataAsCSV23 - errors', () => {
      it('should have a getStatDataRawDataAsCSV23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawDataAsCSV23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCount25 - errors', () => {
      it('should have a getCount25 function', (done) => {
        try {
          assert.equal(true, typeof a.getCount25 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getCount25(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCount25', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountPost25 - errors', () => {
      it('should have a getCountPost25 function', (done) => {
        try {
          assert.equal(true, typeof a.getCountPost25 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFields25 - errors', () => {
      it('should have a getStatDataFields25 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFields25 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatBulkRawData23 - errors', () => {
      it('should have a getStatBulkRawData23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatBulkRawData23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatBulkRawData23 - errors', () => {
      it('should have a getPostStatBulkRawData23 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStatBulkRawData23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatQueryFields25 - errors', () => {
      it('should have a getStatQueryFields25 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatQueryFields25 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionInfoCapture - errors', () => {
      it('should have a getSessionInfoCapture function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionInfoCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disablePacketCaptureSession - errors', () => {
      it('should have a disablePacketCaptureSession function', (done) => {
        try {
          assert.equal(true, typeof a.disablePacketCaptureSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.disablePacketCaptureSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disablePacketCaptureSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadFile - errors', () => {
      it('should have a downloadFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.downloadFile(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forceStopPcapSession - errors', () => {
      it('should have a forceStopPcapSession function', (done) => {
        try {
          assert.equal(true, typeof a.forceStopPcapSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.forceStopPcapSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-forceStopPcapSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startPcapSession - errors', () => {
      it('should have a startPcapSession function', (done) => {
        try {
          assert.equal(true, typeof a.startPcapSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.startPcapSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-startPcapSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileDownloadStatus - errors', () => {
      it('should have a getFileDownloadStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFileDownloadStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getFileDownloadStatus(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getFileDownloadStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopPcapSession - errors', () => {
      it('should have a stopPcapSession function', (done) => {
        try {
          assert.equal(true, typeof a.stopPcapSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.stopPcapSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-stopPcapSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#formPostPacketCapture - errors', () => {
      it('should have a formPostPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.formPostPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.formPostPacketCapture(null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-formPostPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.formPostPacketCapture('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-formPostPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionInfoLog - errors', () => {
      it('should have a getSessionInfoLog function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionInfoLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableDeviceLog - errors', () => {
      it('should have a disableDeviceLog function', (done) => {
        try {
          assert.equal(true, typeof a.disableDeviceLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.disableDeviceLog(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disableDeviceLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadDebugLog - errors', () => {
      it('should have a downloadDebugLog function', (done) => {
        try {
          assert.equal(true, typeof a.downloadDebugLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.downloadDebugLog(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadDebugLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewSessionInfo - errors', () => {
      it('should have a renewSessionInfo function', (done) => {
        try {
          assert.equal(true, typeof a.renewSessionInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.renewSessionInfo(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-renewSessionInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDeviceLog - errors', () => {
      it('should have a searchDeviceLog function', (done) => {
        try {
          assert.equal(true, typeof a.searchDeviceLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.searchDeviceLog(null, null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-searchDeviceLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessions - errors', () => {
      it('should have a getSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearSession - errors', () => {
      it('should have a clearSession function', (done) => {
        try {
          assert.equal(true, typeof a.clearSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.clearSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-clearSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogType - errors', () => {
      it('should have a getLogType function', (done) => {
        try {
          assert.equal(true, typeof a.getLogType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getLogType(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getLogType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamLog - errors', () => {
      it('should have a streamLog function', (done) => {
        try {
          assert.equal(true, typeof a.streamLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logType', (done) => {
        try {
          a.streamLog(null, null, null, null, (data, error) => {
            try {
              const displayE = 'logType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-streamLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.streamLog('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-streamLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.streamLog('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-streamLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLog - errors', () => {
      it('should have a getDeviceLog function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getDeviceLog(null, null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getDeviceLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNwpiDscp - errors', () => {
      it('should have a getNwpiDscp function', (done) => {
        try {
          assert.equal(true, typeof a.getNwpiDscp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNwpiProtocol - errors', () => {
      it('should have a getNwpiProtocol function', (done) => {
        try {
          assert.equal(true, typeof a.getNwpiProtocol === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPacketFeatures - errors', () => {
      it('should have a getPacketFeatures function', (done) => {
        try {
          assert.equal(true, typeof a.getPacketFeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceId', (done) => {
        try {
          a.getPacketFeatures(null, null, null, (data, error) => {
            try {
              const displayE = 'traceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPacketFeatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getPacketFeatures('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPacketFeatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowId', (done) => {
        try {
          a.getPacketFeatures('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getPacketFeatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#traceStart - errors', () => {
      it('should have a traceStart function', (done) => {
        try {
          assert.equal(true, typeof a.traceStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#traceStop - errors', () => {
      it('should have a traceStop function', (done) => {
        try {
          assert.equal(true, typeof a.traceStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceId', (done) => {
        try {
          a.traceStop(null, (data, error) => {
            try {
              const displayE = 'traceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-traceStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceFlow - errors', () => {
      it('should have a getTraceFlow function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceId', (done) => {
        try {
          a.getTraceFlow(null, null, null, (data, error) => {
            try {
              const displayE = 'traceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTraceFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timestamp', (done) => {
        try {
          a.getTraceFlow('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'timestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTraceFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.getTraceFlow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getTraceFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceHistory - errors', () => {
      it('should have a getTraceHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSession - errors', () => {
      it('should have a getSession function', (done) => {
        try {
          assert.equal(true, typeof a.getSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableSpeedTestSession - errors', () => {
      it('should have a disableSpeedTestSession function', (done) => {
        try {
          assert.equal(true, typeof a.disableSpeedTestSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.disableSpeedTestSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-disableSpeedTestSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceBandwidth - errors', () => {
      it('should have a getInterfaceBandwidth function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceBandwidth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuit', (done) => {
        try {
          a.getInterfaceBandwidth(null, null, (data, error) => {
            try {
              const displayE = 'circuit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getInterfaceBandwidth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.getInterfaceBandwidth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getInterfaceBandwidth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startSpeedTest - errors', () => {
      it('should have a startSpeedTest function', (done) => {
        try {
          assert.equal(true, typeof a.startSpeedTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.startSpeedTest(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-startSpeedTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpeedTestStatus - errors', () => {
      it('should have a getSpeedTestStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSpeedTestStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getSpeedTestStatus(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getSpeedTestStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopSpeedTest - errors', () => {
      it('should have a stopSpeedTest function', (done) => {
        try {
          assert.equal(true, typeof a.stopSpeedTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.stopSpeedTest(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-stopSpeedTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveSpeedTestResults - errors', () => {
      it('should have a saveSpeedTestResults function', (done) => {
        try {
          assert.equal(true, typeof a.saveSpeedTestResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.saveSpeedTestResults(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-saveSpeedTestResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.saveSpeedTestResults('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-saveSpeedTestResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpeedTest - errors', () => {
      it('should have a getSpeedTest function', (done) => {
        try {
          assert.equal(true, typeof a.getSpeedTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getSpeedTest(null, null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getSpeedTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processDeviceStatus - errors', () => {
      it('should have a processDeviceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.processDeviceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceUUID', (done) => {
        try {
          a.processDeviceStatus(null, null, (data, error) => {
            try {
              const displayE = 'deviceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-processDeviceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activate - errors', () => {
      it('should have a activate function', (done) => {
        try {
          assert.equal(true, typeof a.activate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterInfo - errors', () => {
      it('should have a getClusterInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocalDC - errors', () => {
      it('should have a deleteLocalDC function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLocalDC === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDC - errors', () => {
      it('should have a deleteDC function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDC === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should have a delete function', (done) => {
        try {
          assert.equal(true, typeof a.delete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails - errors', () => {
      it('should have a getDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReplicationData - errors', () => {
      it('should have a downloadReplicationData function', (done) => {
        try {
          assert.equal(true, typeof a.downloadReplicationData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.downloadReplicationData(null, null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadReplicationData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.downloadReplicationData('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-downloadReplicationData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDisasterRecoveryStatus - errors', () => {
      it('should have a getDisasterRecoveryStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDisasterRecoveryStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistory - errors', () => {
      it('should have a getHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalHistory - errors', () => {
      it('should have a getLocalHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getLocalHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalDataCenterState - errors', () => {
      it('should have a getLocalDataCenterState function', (done) => {
        try {
          assert.equal(true, typeof a.getLocalDataCenterState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pauseDR - errors', () => {
      it('should have a pauseDR function', (done) => {
        try {
          assert.equal(true, typeof a.pauseDR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pauseLocalArbitrator - errors', () => {
      it('should have a pauseLocalArbitrator function', (done) => {
        try {
          assert.equal(true, typeof a.pauseLocalArbitrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pauseLocalDCForDR - errors', () => {
      it('should have a pauseLocalDCForDR function', (done) => {
        try {
          assert.equal(true, typeof a.pauseLocalDCForDR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pauseLocalDCReplication - errors', () => {
      it('should have a pauseLocalDCReplication function', (done) => {
        try {
          assert.equal(true, typeof a.pauseLocalDCReplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryPauseReplication - errors', () => {
      it('should have a disasterRecoveryPauseReplication function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryPauseReplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update - errors', () => {
      it('should have a update function', (done) => {
        try {
          assert.equal(true, typeof a.update === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#register - errors', () => {
      it('should have a register function', (done) => {
        try {
          assert.equal(true, typeof a.register === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteDCMembersState - errors', () => {
      it('should have a getRemoteDCMembersState function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteDCMembersState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteDataCenterState - errors', () => {
      it('should have a getRemoteDataCenterState function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteDataCenterState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDisasterRecoveryState - errors', () => {
      it('should have a updateDisasterRecoveryState function', (done) => {
        try {
          assert.equal(true, typeof a.updateDisasterRecoveryState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteDataCenterVersion - errors', () => {
      it('should have a getRemoteDataCenterVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteDataCenterVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryReplicationRequest - errors', () => {
      it('should have a disasterRecoveryReplicationRequest function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryReplicationRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restartDataCenter - errors', () => {
      it('should have a restartDataCenter function', (done) => {
        try {
          assert.equal(true, typeof a.restartDataCenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDisasterRecoveryLocalReplicationSchedule - errors', () => {
      it('should have a getDisasterRecoveryLocalReplicationSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getDisasterRecoveryLocalReplicationSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdrStatus - errors', () => {
      it('should have a getdrStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getdrStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unpauseDR - errors', () => {
      it('should have a unpauseDR function', (done) => {
        try {
          assert.equal(true, typeof a.unpauseDR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unpauseLocalArbitrator - errors', () => {
      it('should have a unpauseLocalArbitrator function', (done) => {
        try {
          assert.equal(true, typeof a.unpauseLocalArbitrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unpauseLocalDCForDR - errors', () => {
      it('should have a unpauseLocalDCForDR function', (done) => {
        try {
          assert.equal(true, typeof a.unpauseLocalDCForDR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unpauseLocalDCReplication - errors', () => {
      it('should have a unpauseLocalDCReplication function', (done) => {
        try {
          assert.equal(true, typeof a.unpauseLocalDCReplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryUnPauseReplication - errors', () => {
      it('should have a disasterRecoveryUnPauseReplication function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryUnPauseReplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDrState - errors', () => {
      it('should have a updateDrState function', (done) => {
        try {
          assert.equal(true, typeof a.updateDrState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateReplication - errors', () => {
      it('should have a updateReplication function', (done) => {
        try {
          assert.equal(true, typeof a.updateReplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReachabilityInfo - errors', () => {
      it('should have a getReachabilityInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getReachabilityInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawData21 - errors', () => {
      it('should have a getStatDataRawData21 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawData21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsRawData21 - errors', () => {
      it('should have a getStatsRawData21 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsRawData21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAggregationDataByQuery21 - errors', () => {
      it('should have a getAggregationDataByQuery21 function', (done) => {
        try {
          assert.equal(true, typeof a.getAggregationDataByQuery21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostAggregationDataByQuery20 - errors', () => {
      it('should have a getPostAggregationDataByQuery20 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostAggregationDataByQuery20 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentsAsKeyValue - errors', () => {
      it('should have a getComponentsAsKeyValue function', (done) => {
        try {
          assert.equal(true, typeof a.getComponentsAsKeyValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataRawDataAsCSV21 - errors', () => {
      it('should have a getStatDataRawDataAsCSV21 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataRawDataAsCSV21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCount23 - errors', () => {
      it('should have a getCount23 function', (done) => {
        try {
          assert.equal(true, typeof a.getCount23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getCount23(null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getCount23', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountPost23 - errors', () => {
      it('should have a getCountPost23 function', (done) => {
        try {
          assert.equal(true, typeof a.getCountPost23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableEventsFromFile - errors', () => {
      it('should have a enableEventsFromFile function', (done) => {
        try {
          assert.equal(true, typeof a.enableEventsFromFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatDataFields23 - errors', () => {
      it('should have a getStatDataFields23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatDataFields23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListenersInfo - errors', () => {
      it('should have a getListenersInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getListenersInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatBulkRawData21 - errors', () => {
      it('should have a getStatBulkRawData21 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatBulkRawData21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatBulkRawData21 - errors', () => {
      it('should have a getPostStatBulkRawData21 function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStatBulkRawData21 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatQueryFields23 - errors', () => {
      it('should have a getStatQueryFields23 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatQueryFields23 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEventsQueryConfig - errors', () => {
      it('should have a createEventsQueryConfig function', (done) => {
        try {
          assert.equal(true, typeof a.createEventsQueryConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findEvents - errors', () => {
      it('should have a findEvents function', (done) => {
        try {
          assert.equal(true, typeof a.findEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing severityLevel', (done) => {
        try {
          a.findEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'severityLevel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-findEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSeverityHistogram - errors', () => {
      it('should have a getSeverityHistogram function', (done) => {
        try {
          assert.equal(true, typeof a.getSeverityHistogram === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getSeverityHistogram(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-getSeverityHistogram', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventTypesAsKeyValue - errors', () => {
      it('should have a getEventTypesAsKeyValue function', (done) => {
        try {
          assert.equal(true, typeof a.getEventTypesAsKeyValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diffTwoConfigs - errors', () => {
      it('should have a diffTwoConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.diffTwoConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configId1', (done) => {
        try {
          a.diffTwoConfigs(null, null, (data, error) => {
            try {
              const displayE = 'configId1 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-diffTwoConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configId2', (done) => {
        try {
          a.diffTwoConfigs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'configId2 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-viptela-adapter-diffTwoConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
