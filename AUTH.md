## Authenticating Viptela Adapter

This document will go through the steps for authenticating the Viptela adapter with Dynamic Token Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>.

Viptela uses Basic Auth to get a Session AND Token. The adapter will extract the session information out of the Cookie and return that information on the Cookie in all future requests. In addition, the token will be placed into the X-XSRF-TOKEN header in all future request. In addition to the session and token, the Basic Auth is required not only to get the token but on all future requests.

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Dynamic Token Authentication
The Viptela adapter authenticates with a dynamic token.

STEPS
1. Ensure you have access to a Viptela server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "request_token",
  "username": "<username>",
  "password": "<password>",
  "token_timeout": 180000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": [
    "header.headers.Cookie",
    "header.headers.X-XSRF-TOKEN",
    "header.headers.Authorization"
  ],
  "auth_field_format": [
    "JSESSIONID={token}",
    "{tokenp2}",
    "Basic {b64}{username}:{password}{/b64}"
  ],
}
```
you can leave all of the other properties in the authentication section, they will not be used for Viptela dynamic token authentication.
4. Restart the adapter. If your properties were set correctly, the adapter should go online.

### Troubleshooting
- Make sure you copied over the correct username and password as these are used to retrieve the token.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
